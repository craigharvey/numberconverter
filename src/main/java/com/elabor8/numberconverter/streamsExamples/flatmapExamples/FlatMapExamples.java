package com.elabor8.numberconverter.streamsExamples.flatmapExamples;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class FlatMapExamples {

    private static List<Foo> foos = new ArrayList<>();

    public static void main(String[] args) {

        IntStream
                .range(1,4)
                .forEach(i -> foos.add(new Foo("Foo" + i)));

        foos.forEach(f ->
                IntStream
                .range(1,4)
                .forEach(i -> f.bars.add(new Bar("Bar" + i + "<-" + f.name)))
        );

        foos.stream()
                .flatMap(f -> f.bars.stream())
                .forEach(b -> System.out.println(b.name));


        System.out.println("=================");
        IntStream.range(1,4)
                .mapToObj(i -> new Foo("Foo" + i))
                .peek(f -> IntStream.range(1,4)
                    .mapToObj(i -> new Bar("Bar" + i + "<-" + f.name))
                    .forEach(f.bars::add))
                .flatMap(f -> f.bars.stream())
                .forEach(b -> System.out.println(b.name));

    }




}
