package com.elabor8.numberconverter;

class NumberConverter {

    private NumberConverter() {}

    private static String[] unitStrings = {
            "",
            "one",
            "two"
    };

    static String convert(int numberToConvert) {
        System.out.println("Converting : " + numberToConvert + "\n");

        if (isZero(numberToConvert)) return "zero";

        return unitStrings[numberToConvert];
    }

    private static boolean isZero(int numberToConvert) {
        return numberToConvert==0;
    }
}
