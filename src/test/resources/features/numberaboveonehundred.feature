
Feature:
  Convert large numbers again

  @NUM-1
  Scenario:
    Given a number above 100
    When the number is converted
    Then the result is One Hundred
